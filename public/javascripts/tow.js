// Type of Work Mappings

var TOW = 
{
    // Trainee 
    "12" : "(Residents and Clinical Fellows - no moonlighting - includes extra resident shifts and out of province electives)",
    "14" : "(Residents and Clinical Fellows - with moonlighting/restricted registration - includes extra resident shifts and out of province electives)",
    // Practicing code possible for trainees in an assessment program who will treat patients independently

    // Family Medicine or General Practice
    "35" : "Family Medicine or General Practice",
    "73" : "(Family medicine or General practice - primary professional work in family medicine including shifts in the emergency department)",
    "78" : "(Family medicine or General practice including obstetrics - labour and delivery; also includes anesthesia, surgery and shifts in the emergency department)",
    "79" : "(Family medicine or General practice - including anesthesia and surgery; also includes shifts in the emergency department)",
    // Family Medicine or General Practice Exceptions
    "27" : "(Physical Medicine and Rehabilitation/Geriatrics or Palliative medicine)",    
    "37" : "(Surgical consultations/Office surgical practice - also appropriate for physicians whoes practice is restricted to minor cosmetic procedures. If work is restricted to office gynecology, choose 39)",
    "38" : "(Pain medicine without general or spinal anesthesia)",
    "51" : "(Occupational medicine)",    
    "53" : "(Intensive/Critical care)",
    "64" : "(Sport medicine)",    
    "82" : "(Emergency medicine)"
}