$(function() {
    
    var towCode = 0;
    var questionsContainer = $('<div/>').attr('id', 'questions');
    var questionsContainerLabel = $('<h3/>').text('Questions');
    $('#mainContainer').append(questionsContainer.append(questionsContainerLabel));
    
    var counter = 0;
    for (i = 0; i < questions.length; i++) {             
        var question = $('<div/>').addClass('question' + [i]);
        var questionLabel = $('<label/>').text(questions[i].Question);
        var questionOption1 = $('<div/>').addClass('form-check form-check-inline');        
        var questionInputYes = $('<input/>').addClass('form-check-input').attr('type', 'radio').attr('name', 'inlineRadioOptions' + [i]).attr('id', 'inlineRadio' + [counter]).attr('value', questions[i].Answer.Yes);            
        var questionInputLabelYes = $('<label/>').addClass('form-check-label').attr('for', 'inlineRadio' + [counter]).text('Yes');
        counter ++;
        var questionOption2 = $('<div/>').addClass('form-check form-check-inline');        
        var questionInputNo = $('<input/>').addClass('form-check-input').attr('type', 'radio').attr('name', 'inlineRadioOptions' + [i]).attr('id', 'inlineRadio' + [counter]).attr('value', questions[i].Answer.No);            
        var questionInputLabelNo = $('<label/>').addClass('form-check-label').attr('for', 'inlineRadio' + [counter]).text('No');
        counter ++;

        questionOption1.append(questionInputYes).append(questionInputLabelYes);
        questionOption2.append(questionInputNo).append(questionInputLabelNo);
        questionsContainer.append(question.append(questionLabel).append(questionOption1).append(questionOption2).append('<br/><br/>'));        
    }

    // Initial question must be displayed to start wizard
    $('.question0').removeClass('hidden');   
    
    $('input[name=inlineRadioOptions0]').on('change', function() {
        // $('input[name=inlineRadioOptions0]:checked').val()
    });

    
    


    $('#completeWizard').on('click', function() {
        alert('Your TOW code is: ' + towCode);
    });

});