// Questions

var questions = 
[
    {        
        "QuestionNumber": "1",
        "Question" : "Are you a resident, fellow or an international medical graduate in an assessment program?",
        "Answer" : {"Yes" : "Q2", "No" : "Q4"},
        "Required" : "Yes"
    },
    {
        "QuestionNumber": "2",
        "Question" : "Are you an international medical graduate in an assessment program who will be treating patients independently?",
        "Answer" : {"Yes" : "DROPDOWN", "No" : "Q3"},
        "Required" : "No"
    },
    {
        "QuestionNumber": "3",
        "Question" : "Will you be moonlighting outside of your training program for more than two consecutive weeks at a time or billing independently?",
        "Answer" : {"Yes" : "TOW14", "No" : "TOW12"},
        "Required" : "No"
    },    
    {
        // Assumption of TOW35 from here
        "QuestionNumber": "4",
        "Question" : "Do you practice labour and delivery?",
        "Answer" : {"Yes" : "TOW78", "No" : "Q5"},
        "Required" : "No"
    },
    {
        "QuestionNumber": "5",
        "Question" : "Do you administer general or regional anaesthesia or perform surgery under general anaesthesia?",
        "Answer" : {"Yes" : "TOW79", "No" : "Q6"},
        "Required" : "No"
    },
    {
        "QuestionNumber": "6",
        "Question" : "Do you work shifts in an emergency department?",
        "Answer" : {"Yes" : "Q7", "No" : "Q8"},
        "Required" : "No"
    },
    {
        "QuestionNumber": "7",
        "Question" : "Does the majority of your work consist of shifts in an emergency department?",
        "Answer" : {"Yes" : "TOW82", "No" : "TOW73"},
        "Required" : "No"
    },
    {                
        "QuestionNumber": "8",
        "Question" : "Do you work shifts in an intensive care unit?",
        "Answer" : {"Yes" : "TOW35;TOW53", "No" : "Q9"},
        "Required" : "Yes"
    },
    {        
        "QuestionNumber": "9",
        "Question" : "Is your work restricted to geriatric, palliative or physical medicine and rehabilitation?",
        "Answer" : {"Yes" : "TOW27", "No" : "Q10"},
        "Required" : "No"        
    },
    {
        "QuestionNumber": "10",
        "Question" : "Is your work restricted to minor cosmetic procedures?",
        "Answer" : {"Yes" : "TOW37", "No" : "Q11"},
        "Required" : "No"
    },
    {        
        "QuestionNumber": "11",
        "Question" : "Is your work restricted to occupational medicine?",
        "Answer" : {"Yes" : "TOW51", "No" : "Q12"},
        "Required" : "No"
    },
    {
        "QuestionNumber": "12",
        "Question" : "Do you work primarily in Sport and Exercise Medicine?",
        "Answer" : {"Yes" : "TOW64", "No" : "Q13"},
        "Required" : "No"
    },
    {
        "QuestionNumber": "13",
        "Question" : "Do you spend 50% or more of your time working in Pain Medicine?",
        "Answer" : {"Yes" : "TOW35;TOW38", "No" : "END"},
        "Required" : "No"
    }
]